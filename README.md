# Рассылка

>Это приложение для создания рассылок, которое представляет из себя набор запросов на фреймворке Django/DRF. 

#### Развернуть приложение

1. Копируем исходный код: git clone https://gitlab.com/MyBad1231/fablicProject.git
2. Активируем виртуальное окружение(предполагается, что используете linux): virtualenv env && source ./env/bin/activate
3. Устанавливаем инструменты: pip install -r requirements.txt
4. Делаем миграции: python3 manage.py makemigrations && python3 manage.py migrate 
5. Запускаем: python3 manage.py runserver
6. (В другом терминале) celery -A fabricTest worker -l INFO
7. (В другом терминале) celery -A fabricTest -B

#### Запросы

Создать пользователя:
```sh
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"phone": "", "code": "", "teg": "time_zone"}' \
  http://localhost:8000/api/create-user/
```
Обновить пользователя:
```sh
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"id: 0, "phone": "", "code": "", "teg": "time_zone"}' \
  http://localhost:8000/api/update-user/
```
Удалить пользователя:
```sh
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"id: 0}' \
  http://localhost:8000/api/delete-user/
```
Создать рассылку:
```sh
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"text": "", "end": { "date": "12.12.1234", "time": "12:12:12"}, "start": {"date": "13.12.1234", "time": "12:12:12" }, "filter": [{"name": "teg","value": "teg"}]}' \
  http://localhost:8000/api/create-mailing/
```
Удалить рассылку(<id> - id рассылки, которую нужно удалить):
```sh
curl --request GET http://localhost:8000/api/delete-mailing/<id>/
```
(<id> - id рассылки, с которой нужно собрать рассылку):
```sh
curl --request GET http://localhost:8000/api/get-statistic-detailed/<id>/
```
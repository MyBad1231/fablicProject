import requests
import datetime
from app.models import (
    MailingModel, FilterModel,
    ClientModel, MessageModel,
    WorkingLater
)
from django.db.models.query import QuerySet


class MyIdError(Exception):
    """error for getting id of MailingModel"""
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return 'MyIdError, {0}'.format(self.message)
        else:
            return 'MyIdError has been raised'


class MailingWorker:
    """utils and logic for work with mailing"""

    def __init__(self, id_obj):
        # control type of id_obj
        try:
            int(id_obj)
        except ValueError:
            raise MyIdError('id of object is not number')

        # get obj by id_obj
        try:
            self.mailing = MailingModel.objects.get(id=id_obj)
        except MailingModel.DoesNotExist:
            raise MyIdError('MailingModel object with this id is not found')

    def get_phone_by_filter(self):
        """get ClientModel objects by filter"""

        all_obj = ClientModel.objects.all()
        for i in FilterModel.objects.filter(mailing=self.mailing):
            if i.name == 'phone':
                all_obj = all_obj.filter(phone__contains=i.value)

            if i.name == 'code':
                all_obj = all_obj.filter(code__contains=i.value)

            if i.name == 'teg':
                all_obj = all_obj.filter(teg__contains=i.value)

            if i.name == 'time_zone':
                all_obj = all_obj.filter(time_zone__contains=i.value)

        return all_obj

    def create_start_data_message(self, data: QuerySet):
        """create starting data for creating MessageModel"""

        for i in data:
            MessageModel.objects.create(
                status='no_queue', mailing=self.mailing,
                client=i
            )

        return MessageModel.objects.filter(mailing=self.mailing)

    def send_message_api(self, id_msg, phone, text):
        """work with request for sending of mailing"""

        # work with url
        url = 'https://probe.fbrq.cloud/v1/send/{0}'.format(
            str(id_msg)
        )

        # create struct data of json
        data = {
            'id': int(id_msg),
            'phone': int(phone),
            'text': text
        }

        headers = {'Authorization': 'Bearer ' + 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTE0OTcxMDQsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IlRlUTFMYV9Xb1cifQ.San-58DW0_A4sj_ae2T02POU-N2-AdiXfjzmMbqFi_U'}

        # send request
        request = requests.post(url, headers=headers, json=data)

        if request.status_code == 200:
            return True
        else:
            return False

    def sending_message(self, data: QuerySet):
        """make request to other service for mailing on phone"""

        for i in data:
            # set datetime for work with mailing
            dt_end = self.mailing.end.timestamp()
            dt_now = datetime.datetime.now().timestamp()

            # work with objects of data
            if dt_end < dt_now:
                i.status = 'no_end'
                i.save()
            else:
                # make request to company api
                status = self.send_message_api(
                    i.id, i.client.phone,
                    self.mailing.text
                )

                if not status:
                    WorkingLater.objects.create(
                        message=i
                    )
                    return

                i.status = 'yes'
                i.save()

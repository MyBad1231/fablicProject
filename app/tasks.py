from celery import shared_task

from .utils import MailingWorker
from .models import (
    MailingModel, FilterModel,
    ClientModel, MessageModel,
    WorkingLater
)


@shared_task()
def worker(id_mailing):
    # get phones for mailing
    a = MailingWorker(id_mailing)
    data = a.get_phone_by_filter()

    # work with mailing
    messages = a.create_start_data_message(data)
    a.sending_message(messages)


@shared_task()
def periodic():
    """work with mailing if service with api don't work"""

    for i in WorkingLater.objects.all():
        status = MailingWorker.send_message_api(
            id_msg=i.message.id,
            phone=i.message.client.phone,
            text=i.message.mailing.text
        )
        if status:
            i.message.status = 'yes'
            i.message.save()

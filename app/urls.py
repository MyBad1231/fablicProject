from django.urls import path

from app.views.clinet_views import *
from app.views.mailing_views import *

urlpatterns = [
    path('create-user/', ClientView.as_view({'post': 'create_user'})),
    path('update-user/', ClientView.as_view({'post': 'update_user'})),
    path('delete-user/', ClientView.as_view({'post': 'delete_user'})),
    path('create-mailing/', MailingView.as_view({'post': 'create_mailing'})),
    path('delete-mailing/<int:id_obj>/', MailingView.as_view({'get': 'delete_mailing'})),
    path('get-statistic/', MailingView.as_view({'get': 'get_general_statistic'})),
    path('get-statistic-detailed/<int:id_obj>/', MailingView.as_view({'get': 'get_statistic_by_id'})),
    path('update-mailing/', MailingView.as_view({'post': 'update_mailing'})),

    path('test/<int:id>/', MailingView.as_view({'get': 'test'})),
]

from rest_framework import viewsets, status
from rest_framework.response import Response

from app.models import ClientModel


class ClientSerializer:
    """work with data of client"""

    def __init__(self):
        self.data = None

    def set_data(self, data):
        self.data = data

    def get_data(self):
        return self.data

    def work_with_phone(self):
        """control field phone"""

        # work with types
        if type(self.data) != dict:
            return False

        if type(self.data.get('phone')) != str:
            return False

        # work with content of data
        if self.data.get('phone')[0] != '7':
            return False

        list_numbers = [
            '0', '1', '3', '4', '5',
            '6', '7', '8', '9', '2',
        ]
        for i in self.data.get('phone'):
            if i not in list_numbers:
                return False

        if len(self.data.get('phone')) != 11:
            return False

        return True

    def work_with_code(self):
        """work with code field"""

        # work with types
        if type(self.data) != dict:
            return False

        if type(self.data.get('code')) != str:
            return False

        if len(self.data.get('code')) < 1:
            return False

        return True

    def work_with_teg(self):
        """work with teg field"""

        # work with types
        if type(self.data) != dict:
            return False

        if type(self.data.get('teg')) != str:
            return False

        if len(self.data.get('teg')) < 1:
            return False

        return True

    def work_with_time_zone(self):
        """work with time_zone field"""

        # work with types
        if type(self.data) != dict:
            return False

        if type(self.data.get('time_zone')) != str:
            return False

        if len(self.data.get('time_zone')) < 0:
            return False

        return True

    def work_with_all_fields(self):
        """work with all fields of client data"""

        if not self.work_with_phone():
            return False

        if not self.work_with_code():
            return False

        if not self.work_with_teg():
            return False

        if not self.work_with_time_zone():
            return False

        return True


class ClientUtils:
    """some utils for working with clients data"""

    @staticmethod
    def get_data_from_model(data: ClientModel):
        """get data from ClientModel model"""

        return {
            'id': data.id,
            'phone': data.phone,
            'code': data.code,
            'teg': data.teg,
            'time_zone': data.time_zone
        }

    @staticmethod
    def update_data_client_obj(obj: ClientModel, data: dict):
        """update data of client and save to db"""

        # save data to db
        obj.phone = data.get('phone')
        obj.teg = data.get('teg')
        obj.code = data.get('code')
        obj.time_zone = data.get('time_zone')
        obj.save()

        return {
            'id': obj.id,
            'phone': obj.phone,
            'code': obj.code,
            'teg': obj.teg,
            'time_zone': obj.time_zone
        }


class ClientView(viewsets.ViewSet):
    """class for work with client data"""

    def create_user(self, request):
        """create user"""

        # send data to serializer
        serializer = ClientSerializer()
        serializer.set_data(request.data)

        # control valid data or no
        if not serializer.work_with_all_fields():
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'message': 'error of data or type of data'
            })

        # save new
        client_obj = ClientModel(**serializer.get_data())
        client_obj.save()

        return Response(
            data=ClientUtils.get_data_from_model(client_obj)
        )

    def update_user(self, request):
        """update data of client by id"""

        # get id from request
        try:
            number = request.data.get('id')
            int(number)
        except TypeError:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'message': 'id is not integer'
            })

        # get client by id
        try:
            client_obj = ClientModel.objects.get(id=number)
        except ClientModel.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'message': 'not found client with this id'
            })

        # send data to serializer
        serializer = ClientSerializer()
        serializer.set_data(request.data)

        # control valid data or no
        if not serializer.work_with_all_fields():
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'message': 'error of data or type of data'
            })

        return Response(
            data=ClientUtils.update_data_client_obj(client_obj, serializer.get_data())
        )

    def delete_user(self, request):
        """delete client from db"""

        # get id from request
        try:
            number = request.data.get('id')
            int(number)
        except TypeError:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'message': 'id is not integer'
            })

        try:
            client_obj = ClientModel.objects.get(id=number)
            client_obj.delete()

            return Response()

        except ClientModel.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'message': 'not found client with this id'
            })

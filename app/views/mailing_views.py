import datetime
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.request import Request

from app.models import (
    MailingModel, FilterModel, MessageModel
)
from app.utils import MailingWorker


class MailingSerializer:
    """class for working with mailing s data"""

    def __init__(self, data):
        self.data = data
        self.numbers = [
            '0', '1', '2', '3', '4',
            '5', '6', '7', '8', '9'
        ]

    def work_with_text(self):
        """work with text field"""

        # work with types
        if type(self.data) != dict:
            return False

        if type(self.data.get('text')) != str:
            return False

        # work with content
        if len(self.data.get('text')) < 1:
            return False

        return True

    def work_with_start_field(self):
        """work with start field"""

        # work with types
        if type(self.data) != dict:
            return False

        if type(self.data.get('start')) != dict:
            return False

        # work with content of start
        if type(self.data.get('start').get('date')) != str:
            return False

        if type(self.data.get('start').get('time')) != str:
            return False

        # work with date
        date = self.data.get('start').get('date')
        time = self.data.get('start').get('time')
        if not self.work_with_datetime(date, time):
            return False

        return True

    def work_with_end_field(self):
        """work with end field"""

        # work with types
        if type(self.data) != dict:
            return False

        if type(self.data.get('end')) != dict:
            return False

        # work with content of start
        if type(self.data.get('end').get('date')) != str:
            return False

        if type(self.data.get('end').get('time')) != str:
            return False

        # work with date and time
        date = self.data.get('end').get('date')
        time = self.data.get('end').get('time')
        if not self.work_with_datetime(date, time):
            return False

        return True

    def work_with_datetime(self, date: str, time: str):
        """work with datetime struct of end and start fields"""

        # work with date and time
        try:
            date_struct = {
                'day': date.split('.')[0],
                'month': date.split('.')[1],
                'year': date.split('.')[2]
            }
        except IndexError:
            return False

        # work with types of struct
        try:
            int(date_struct.get('day'))
            int(date_struct.get('month'))
            int(date_struct.get('year'))
        except ValueError:
            return False

        # control numbers for date
        try:
            datetime.date(
                day=int(date_struct.get('day')),
                month=int(date_struct.get('month')),
                year=int(date_struct.get('year'))
            )
        except ValueError:
            return False

        # work with time
        try:
            time_struct = {
                'hour': time.split(':')[0],
                'minute': time.split(':')[1],
                'second': time.split(':')[2]
            }
        except IndexError:
            return False

        # work with types of struct
        try:
            int(time_struct.get('hour'))
            int(time_struct.get('minute'))
            int(time_struct.get('second'))
        except ValueError:
            return False

        # control numbers for time
        try:
            datetime.time(
                hour=int(time_struct.get('hour')),
                minute=int(time_struct.get('minute')),
                second=int(time_struct.get('second'))
            )
        except ValueError:
            pass

        return True

    def work_with_filters(self):
        """work with fields of FilterModel"""

        # work with types
        if type(self.data) != dict:
            return False

        if type(self.data.get('filter')) != list:
            return False

        # work with content of list
        for i in self.data.get('filter'):
            if type(i) != dict:
                return False

            if type(i.get('name')) != str:
                return False

            if type(i.get('value')) != str:
                return False

        return True

    def work_with_all_fields(self):
        """work with all fields of MailingModel"""

        if not self.work_with_text():
            return False
        if not self.work_with_end_field():
            return False
        if not self.work_with_start_field():
            return False
        if not self.work_with_filters():
            return False

        return True

    def get_filters(self):
        """get field with filter"""

        # work with fields for FilterModel
        filter_arr_correct = []
        for i in self.data.get('filter'):
            if i.get('name') == 'phone':
                filter_arr_correct.append(i)

            if i.get('name') == 'code':
                filter_arr_correct.append(i)

            if i.get('name') == 'teg':
                filter_arr_correct.append(i)

            if i.get('name') == 'time_zone':
                filter_arr_correct.append(i)

        return filter_arr_correct

    def get_data(self):
        """get data for saving to db(MailingModel, FilterModel)"""

        # work with start field
        start_datetime = datetime.datetime(
            year=int(self.data.get('start').get('date').split('.')[2]),
            month=int(self.data.get('start').get('date').split('.')[1]),
            day=int(self.data.get('start').get('date').split('.')[0]),
            hour=int(self.data.get('start').get('time').split(':')[0]),
            minute=int(self.data.get('start').get('time').split(':')[1]),
            second=int(self.data.get('start').get('time').split(':')[2])
        )

        # work with end field
        end_datetime = datetime.datetime(
            year=int(self.data.get('end').get('date').split('.')[2]),
            month=int(self.data.get('end').get('date').split('.')[1]),
            day=int(self.data.get('end').get('date').split('.')[0]),
            hour=int(self.data.get('end').get('time').split(':')[0]),
            minute=int(self.data.get('end').get('time').split(':')[1]),
            second=int(self.data.get('end').get('time').split(':')[2])
        )

        return {
            'text': self.data.get('text'),
            'start': start_datetime,
            'end': end_datetime,
            'filter': self.get_filters()
        }

    def get_data_for_update(self):
        """get data for update data in MailingModel and FilterModel"""

        return {
            'text': self.data.get('text'),
            'filter': self.get_filters()
        }


class MailingUtils:
    """some utils for working with mailing data"""

    @staticmethod
    def save_data(data):
        """save data to MailingModel and FilterModel"""

        # save to MailingModel
        mailing_obj = MailingModel.objects.create(
            text=data.get('text'),
            start=data.get('start'),
            end=data.get('end')
        )

        # save to FilterModel
        for i in data.get('filter'):
            FilterModel.objects.create(
                name=i.get('name'),
                value=i.get('value'),
                mailing=mailing_obj
            )

    @staticmethod
    def save_data_update(obj: MailingModel, data: dict):
        """update data for mailing"""

        # save new text
        obj.text = data.get('text')
        obj.save()

        # save new filters
        for i in FilterModel.objects.filter(mailing=obj):
            i.delete()

        for i in data.get('filter'):
            FilterModel.objects.create(
                name=i.get('name'),
                value=i.get('value'),
                mailing=obj
            )

    @staticmethod
    def get_mailing_by_request(request: Request):
        """get data by id from DRF framework request"""

        # get id
        try:
            id_obj = request.data.get('id')
            int(id_obj)
        except TypeError:
            return None

        # get object from MailingModel
        try:
            mailing_obj = MailingModel.objects.get(id=id_obj)
        except MailingModel.DoesNotExist:
            return None

        return mailing_obj

    @staticmethod
    def get_messages_by_mailing_id(id_mailing: int):
        """get messages by id of mailing"""

        # control type of id_mailing
        try:
            int(id_mailing)
        except TypeError:
            return {
                'no_queue': [],
                'no_end': [],
                'yes': []
            }

        # control object of MailingModel
        try:
            mailing_obj = MailingModel.objects.get(id=id_mailing)
        except MailingModel.DoesNotExist:
            return {
                'no_queue': [],
                'no_end': [],
                'yes': []
            }

        # work with messages
        data = {}

        # work with messages(in queue)
        for_messages = []
        messages = MessageModel.objects.filter(
            mailing=mailing_obj, status='no_queue'
        )
        for i in messages:
            for_messages.append(i.client.phone)

        data.update({'no_queue': for_messages})

        # work with messages(is end)
        for_messages = []
        messages = MessageModel.objects.filter(
            mailing=mailing_obj, status='no_end'
        )
        for i in messages:
            for_messages.append(i.client.phone)

        data.update({'no_queue': for_messages})

        # work with messages(stopped)
        for_messages = []
        messages = MessageModel.objects.filter(
            mailing=mailing_obj, status='yes'
        )
        for i in messages:
            for_messages.append(i.client.phone)

        data.update({'no_queue': for_messages})

        return data


class MailingView(viewsets.ViewSet):
    """class for work with Mailing by user s requests"""

    def create_mailing(self, request):
        """send new data about new mailing to db"""

        # init new serializer for work with data and control
        serializer = MailingSerializer(request.data)
        if not serializer.work_with_all_fields():
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={'message': 'bad struct for data of your request'}
            )

        # save data to MailingModel and FilterModel
        MailingUtils.save_data(serializer.get_data())

        return Response()

    def delete_mailing(self, request, id_obj):
        """delete mailing by id"""

        try:
            mailing = MailingModel.objects.get(id=id_obj)
            mailing.delete()
        except MailingModel.DoesNotExist:
            pass

        return Response()

    def get_general_statistic(self, request):
        """get info about all mailing"""

        # get statistic by mailing
        data = []
        for i in MailingModel.objects.all():
            # get all filters for this mailing
            filter_objects = FilterModel.objects.filter(
                mailing=i
            )

            # get struct of data for this filters
            filters = []
            for j in filter_objects:
                filters.append({
                    'name': j.name, 'value': j.value
                })

            data.append({
                'id': i.id,
                'text': i.text,
                'start': i.start,
                'end': i.end,
                'filters': filters,
                'messages': {
                    'no_queue': len(MessageModel.objects.filter(
                        mailing=i, status='no_queue'
                    )),
                    'no_end': len(MessageModel.objects.filter(
                        mailing=i, status='no_end'
                    )),
                    'yes': len(MessageModel.objects.filter(
                        mailing=i, status='yes'
                    ))
                }
            })

        return Response(data=data)

    def get_statistic_by_id(self, request, id_obj):
        """get all info about mailing by id"""

        # get mailing
        try:
            mailing_obj = MailingModel.objects.get(id=id_obj)
        except MailingModel.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'message': 'incorrect id'
            })

        filters = []
        for i in FilterModel.objects.filter(mailing=mailing_obj):
            filters.append({
                'name': i.name, 'value': i.value
            })

        return Response(data={
            'text': mailing_obj.text,
            'start': mailing_obj.start,
            'end': mailing_obj.end,
            'filters': filters,
            'messages': MailingUtils.get_messages_by_mailing_id(id_obj)
        })

    def update_mailing(self, request):
        """update text or filter of mailing by id"""

        # get MailingModel object by id
        mailing_obj = MailingUtils.get_mailing_by_request(request)
        if mailing_obj is None:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'message': 'incorrect id'
            })

        # control time of MailingModel object
        dt_now = datetime.datetime.now()
        if dt_now.timestamp() > mailing_obj.start.timestamp():
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'message': 'this mailing working already'
            })

        # control valid data for update or no
        serializer = MailingSerializer(data=request.data)
        if not serializer.work_with_text():
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'message': 'bad text field in request'
            })

        if not serializer.work_with_filters():
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'message': 'bad text field in filter'
            })

        # save updating data to db
        MailingUtils.save_data_update(
            mailing_obj, serializer.get_data_for_update()
        )

        return Response()

    def test(self, request, id):

        a = MailingWorker(id)
        data = a.get_phone_by_filter()
        messages = a.create_start_data_message(data)
        a.sending_message(messages)

        return Response()

from django.db import models


class MailingModel(models.Model):
    """model from mailing"""

    text = models.TextField()
    start = models.DateTimeField()
    end = models.DateTimeField()


class FilterModel(models.Model):
    """filter of users for mailing"""

    name = models.CharField(max_length=200)
    value = models.CharField(max_length=200)
    mailing = models.ForeignKey(MailingModel, on_delete=models.CASCADE)


class ClientModel(models.Model):
    """model for client"""

    phone = models.CharField(max_length=150)
    code = models.CharField(max_length=200)
    teg = models.CharField(max_length=200)
    time_zone = models.CharField(max_length=200)


class MessageModel(models.Model):
    """
    model for data of message
    status field: no_queue, no_end, yes
    """

    datetime_sending = models.DateTimeField(
        null=True,
        blank=True,
        default=None
    )

    status = models.CharField(max_length=100)
    mailing = models.ForeignKey(MailingModel, on_delete=models.CASCADE)
    client = models.ForeignKey(ClientModel, on_delete=models.CASCADE)


class WorkingLater(models.Model):
    """work with mailing latter (if service is not work)"""

    message = models.ForeignKey(MessageModel, on_delete=models.CASCADE)
